var React = require('react-native');

var {
    StyleSheet,
    Text,
    View
    } = React;

var Forecast = React.createClass({
    render: function() {
        return (
            <View>
                <Text style={styles.firstText}>Country: {this.props.country}
                </Text>
                <Text style={styles.mainText}>City: {this.props.city}
                </Text>
                <Text style={styles.bigText}>Weather: {this.props.temp}°C
                </Text>
                <Text style={styles.bigText}>Weather: {this.props.main}
                </Text>
                <Text style={styles.mainText}>Current conditions: {this.props.description} </Text>

                <Text style={styles.bigText}></Text>
            </View>
        );
    }
});

var styles = StyleSheet.create({
    bigText: {
        flex: 2,
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: '#FFFFFF'
    },
    mainText: {
        flex: 1,
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#FFFFFF'
    },
    firstText: {
        flex: 1,
        fontSize: 16,
        textAlign: 'center',
        marginTop: -5,
        marginBottom: 10,
        marginLeft: 10,
        color: '#FFFFFF'
    }
});

module.exports = Forecast;