'use strict';

const React = require('react-native');

const {
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Alert
    } = React;

const Forecast = require('./Forecast');
const Login = require('./Login');
const AuthService = require('./AuthService');

const MyWeatherProject = React.createClass({

    componentDidMount: function(){
        AuthService.getAuthInfo((err, authInfo)=> {
            this.setState({
                checkingAuth: false,
                isLoggedIn: false/*authInfo != null*/
            })
        });
    },
    getInitialState: function() {
        return {
            place: '',
            forecast: null,
            isLoggedIn: false,
            checkingAuth: true
        };
    },
    // We'll pass this callback to the <TextInput>
    _handleTextChange(event) {
        const place = event.nativeEvent.text;

        if(place.length < 1) {
            Alert.alert('You must type a place', '', [{ text: 'OK' }]);
            return;
        }

        /*
         https://facebook.github.io/react-native/docs/network.html

         LOGIN
         fetch('http://localhost:3000/api/tasks/', {
         method: 'POST',
         headers: {
         'Accept': 'application/json',
         'Content-Type': 'application/json',
         },
         body: JSON.stringify({
         username: 'bob@mail.dk',
         password: 'kodeord',
         })
         })



         */

        this.setState({place: place});
        fetch('http://localhost:3000/api/login/', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: 'bob@mail.dk',
                password: 'kodeord'
            })
        })
            .then((response) => {
                const json = response._bodyInit;
                const status = json.status;
                const data = json.data;
                const authToken = data.authToken;
                const userId = data.userId;
            })
            .catch((error) => {
                console.warn(error);
            });
    },
    render() {
        if(this.state.isLoggedIn) {

            var content = null;
            if (this.state.forecast !== null) {
                content = <Forecast country={this.state.forecast.country}
                                    city={this.state.forecast.city}
                                    main={this.state.forecast.main}
                                    description={this.state.forecast.description}
                                    temp={this.state.forecast.temp}/>;
            }
            // For debugging
            //content = <Forecast country={'Denmark'}
            //                            city={'Copenhagen'}
            //                            main={'Sunny'}
            //                            description={'Sunshine reggae'}
            //                            temp={'30'}/>;
            return (
                <View style={styles.container}>
                    <Image source={require('image!flowers')}
                           resizeMode='cover'
                           style={styles.backdrop}>
                        <View style={styles.overlay}>
                            <View style={styles.row}>
                                <Text style={styles.mainText}>Current weather for
                                </Text>
                                <View style={styles.textInputContainer}>
                                    <TextInput
                                        style={[styles.textInput, styles.mainText]}
                                        onSubmitEditing={this._handleTextChange}/>
                                </View>
                            </View>
                            {content}
                        </View>
                    </Image>
                </View>
            );
        }
        else {
            return (<Login onLogin={this.onLogin} />);
        }
    },
    onLogin: function(){
        this.setState({isLoggedIn: true});
    }
});

const baseFontSize = 16;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 30
    },
    backdrop: {
        flex: 1,
        flexDirection: 'column'
    },
    overlay: {
        paddingTop: 5,
        backgroundColor: '#000000',
        opacity: 0.5,
        flexDirection: 'column',
        alignItems: 'center'
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'nowrap',
        alignItems: 'flex-start',
        paddingTop: 25,
        paddingBottom: 25
    },
    textInputContainer: {
        flex: 1,
        borderBottomColor: '#DDDDDD',
        borderBottomWidth: 1,
        marginLeft: 5,
        marginTop: 3
    },
    textInput: {
        width: 120,
        height: baseFontSize
    },
    mainText: {
        flex: 1,
        fontSize: baseFontSize,
        color: '#FFFFFF'
    }
});

module.exports = MyWeatherProject;