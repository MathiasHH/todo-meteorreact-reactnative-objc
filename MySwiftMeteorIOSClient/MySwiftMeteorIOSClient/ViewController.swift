//
//  ViewController.swift
//  MySwiftMeteorIOSClient
//
//  Created by Mathias Hansen on 13/05/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

import UIKit
import SwiftDDP

class ViewController: UIViewController {

	 let tasks = MeteorCollection<Todo>(name: "tasks")
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	
//		view.backgroundColor = UIColor.whiteColor()
		
		let loginButton = UIButton()
		loginButton.setTitle("Login"/*"✸"*/, forState: .Normal)
		loginButton.setTitleColor(UIColor.blueColor(), forState: .Normal)
		loginButton.frame = CGRectMake(15, 100, 100, 100)
		loginButton.addTarget(self, action: #selector(loginBtnPressed(_:)), forControlEvents: .TouchUpInside)
	
		let signupButton = UIButton()
		signupButton.setTitle("Signup"/*"✸"*/, forState: .Normal)
		signupButton.setTitleColor(UIColor.blueColor(), forState: .Normal)
		signupButton.frame = CGRectMake(120, 100, 100, 100)
		signupButton.addTarget(self, action: #selector(signupBtnPressed(_:)), forControlEvents: .TouchUpInside)
		
		let myFirstLabel = UILabel()
		myFirstLabel.text = "I made a label on the screen #toogood4you"
		myFirstLabel.font = UIFont(name: "MarkerFelt-Thin", size: 45)
		myFirstLabel.textColor = UIColor.redColor()
		myFirstLabel.textAlignment = .Center
		myFirstLabel.numberOfLines = 5
		myFirstLabel.frame = CGRectMake(15, 54, 300, 500)
	
		view.addSubview(myFirstLabel)
		view.addSubview(loginButton)
		view.addSubview(signupButton)
		
		
		let printListBtn = UIButton()
		printListBtn.frame = CGRectMake(100, 400, 100, 100)
		printListBtn.setTitle("print List", forState: .Normal)
		printListBtn.setTitleColor(UIColor.blackColor(), forState: .Normal)
		printListBtn.addTarget(self, action: #selector(printList), forControlEvents: .TouchUpInside)
		view.addSubview(printListBtn)
		
	}
	
	func printList()
	{
		print("printList called")
		print(tasks.count)
		
		
//		if let t : Todo = tasks.findOne("n5NfF7r3JMjPLK8A4")
//		{
//			print("props : \(t.propertyNames())")
//			t.fields()
		
			
//			if let v = t.id {
//				print (v)
//			}
//			if let v = t.text {
//				print (v)
//			}
//			if let v = t.createdAt {
//				print (v)
//			}
//			if let v = t.owner {
//				print (v)
//			}
//			if let v = t.username {
//				print (v)
//			}
//			if let v = t.isPrivate {
//				print (v)
//			}
//			if let v = t.checked{
//				print (v)
//			}
//		}
	}

	func loginBtnPressed(sender: UIButton!) {
		
		let username = "post@mathiashh.dk"
		let password = "lydiehansen"
		
		Meteor.loginWithUsername(username, password: password) { result, error in
			if (error == nil) {
//				self.dismissViewControllerAnimated(true, completion: nil)
				print("login succesfully")
			} else {
				if let reason = error?.reason {
					print(reason)
				}
			}
		}
	}

	func signupBtnPressed(sender: UIButton!) {
		
		let username = "pixi@mathiashh.dk"
		let password = "123456"
		
		Meteor.signupWithUsername(username, password: password) { result, error in
			if (error == nil) {
				//				self.dismissViewControllerAnimated(true, completion: nil)
				print("signup succesfully")
			} else {
				if let reason = error?.reason {
					print(reason)
				}
			}
		}
	}
	
//	override func viewWillDisappear(animated: Bool) {
//		super.viewWillDisappear(animated)
//		NSNotificationCenter.defaultCenter().removeObserver(self)
//		
//	}

}

