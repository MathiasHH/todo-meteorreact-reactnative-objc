//
//  AppDelegate.swift
//  MySwiftMeteorIOSClient
//
//  Created by Mathias Hansen on 13/05/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

import UIKit
import SwiftDDP

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var window: UIWindow?
	
	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		// Override point for customization after application launch.
	
		
		//		noStoryBoard() // remember to remove set initial controller in interface builder if not using storyboard and main in Info.plist
		
	
		Meteor.client.allowSelfSignedSSL = true
		Meteor.client.logLevel = .Info
				
//		Meteor.connect("ws://localhost:3000/websocket") {/* do something after the client connects */ }
		Meteor.connect("wss://139.59.159.223/websocket") {/* do something after the client connects */ }
		
		return true
	}
	
	func noStoryBoard()
	{
		window = UIWindow(frame: UIScreen.mainScreen().bounds)
		let viewController = ViewController()
		window!.rootViewController = viewController
		window!.backgroundColor = UIColor.whiteColor()
		window!.makeKeyAndVisible()
	}
	
	func applicationWillResignActive(application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	}
	
	func applicationDidEnterBackground(application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	}
	
	func applicationWillEnterForeground(application: UIApplication) {
		// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	}
	
	func applicationDidBecomeActive(application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}
	
	func applicationWillTerminate(application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}
	
	
}

