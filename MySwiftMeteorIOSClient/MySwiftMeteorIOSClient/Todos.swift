
import UIKit
import SwiftDDP

// Allows us to attach the list _id to the cell
public class TodoCell:UITableViewCell {
    var _id:String?
}

class Todos: UITableViewController {
    
	let tasks = MeteorCollection<Todo>(name: "tasks")
	
    var listId:String? {
        didSet {
            Meteor.subscribe("tasks", params: [listId!]) {
                self.tableView.reloadData()
            }
        }
    }
	
	func callMethodAtServer(text: String){
		Meteor.call("methodCalledFromApp", params: [text, 22, 33, 44]) { result, error in
			// Do something with the method result
		}
	}
	
    @IBOutlet weak var addTaskTextField: UITextField!
    
    // Insert the todo
    @IBAction func add(sender: UIButton) {

		if let task = addTaskTextField.text where task != "" {
			callMethodAtServer(task)
		}
		
        if let task = addTaskTextField.text where task != "" {
            let _id = Meteor.client.getId()
//            let todo = Todo(id:_id, fields: ["listId":listId!, "text":task])
			let todo = Todo(id: _id, fields: ["text":task])
            tasks.insert(todo)
            addTaskTextField.text = ""
            self.tableView.reloadData()
        }
    }
	
    override func viewDidLoad() {
        super.viewDidLoad()

        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(reloadTableView), name: METEOR_COLLECTION_SET_DID_CHANGE, object: nil)
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
        Meteor.unsubscribe("tasks")
    }
//
    func reloadTableView() {
//		print("Number of tasks received from Backend : \(tasks.count)")

        self.tableView.reloadData()
		
//		if !isLoggedIn {
//			Authentication().login()
//			Authentication().signup()
//		}
    }

	
	// MARK: - Table view data source
	
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return tasks.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("todoCell", forIndexPath: indexPath) as! TodoCell
        
        let todo = tasks.sorted[indexPath.row]
        if let checked = todo.valueForKey("checked") where checked as! Bool == true {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        cell.textLabel?.text = todo.valueForKey("text") as? String
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let object = tasks.sorted[indexPath.row]
        object.checked = !object.checked
        tasks.update(object)
        self.tableView.reloadData()
    }

    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            let object = tasks.sorted[indexPath.row]
            tasks.remove(object)
            tableView.reloadData()
        }
    }

 }
