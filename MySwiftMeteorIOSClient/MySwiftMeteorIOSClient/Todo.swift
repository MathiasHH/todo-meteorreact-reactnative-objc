//
//  Todo.swift
//  MySwiftMeteorIOSClient
//
//  Created by Mathias Hansen on 14/05/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

import Foundation
import SwiftDDP

class Todo: MeteorDocument {
	
//	var collection:String = "todos"
	var id:String?
	var text:String?
	var createdAt:NSDate?
	var owner:String?
	var username:String?
	var isPrivate:Bool = false
	var checked:Bool = false
	
}