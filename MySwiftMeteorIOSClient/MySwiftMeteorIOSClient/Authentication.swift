//
//  Authentication.swift
//  MySwiftMeteorIOSClient
//
//  Created by Mathias Hansen on 17/05/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

import UIKit
import SwiftDDP

	var isLoggedIn = false

class Authentication: NSObject {

	// MARK: Sigup
	
	func signup() {
		let email = "bob@mail.dk"
		let password = "kodeord"
		
		Meteor.signupWithUsername(email, password: password, email: email, profile: nil) { (result, error) in
			if (error == nil) {
				print ("signed up succesfully")
				Meteor.client.loginWithToken() { result, error in
					if (error == nil) {
						print("Logged In with Token")
						isLoggedIn = true;
					}
				}
			}
			else {
				print("Error when trying to signup")
			}
		}
	}

	// MARK: Login
	
	func login()
	{
		let username = "bob@mail.dk"
		let password = "kodeord"
		
		Meteor.loginWithUsername(username, password: password) { (result, error) in
			if error == nil {
				//				self.dismissViewControllerAnimated(true, completion: nil)
				print("login succesfully")
				isLoggedIn = true
				
			} else {
				if let reason = error?.reason {
					print(reason)
				}
			}
			
		}
	}
	
}
