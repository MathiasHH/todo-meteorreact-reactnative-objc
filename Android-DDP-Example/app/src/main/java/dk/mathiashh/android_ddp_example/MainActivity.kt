package dk.mathiashh.android_ddp_example

import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.view.Menu
import android.view.MenuItem

import im.delight.android.ddp.Meteor
import im.delight.android.ddp.MeteorCallback

class MainActivity : AppCompatActivity(), MeteorCallback {

    private var mMeteor: Meteor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById(R.id.toolbar) as Toolbar?
        setSupportActionBar(toolbar)

        // create a new instance
        mMeteor = Meteor(this, "ws://139.59.159.223/websocket")

        // register the callback that will handle events and receive messages
        mMeteor!!.addCallback(this)

        // establish the connection
        mMeteor!!.connect()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onConnect(signedInAutomatically: Boolean) {
        print("Connected")
        println("Is logged in: " + mMeteor!!.isLoggedIn)
        println("User ID: " + mMeteor!!.userId)


        // subscribe to data from the server
        val subscriptionId = mMeteor!!.subscribe("tasks")
    }

    override fun onDisconnect() {
        println("Disconnected")
    }

    override fun onException(e: Exception?) {
        println("Exception")
        e?.printStackTrace()
    }

    override fun onDataAdded(collectionName: String, documentID: String, newValuesJson: String) {
        println("Data changed in <$collectionName> in document <$documentID>")
        println("json : " + newValuesJson)
    }

    override fun onDataChanged(collectionName: String, documentID: String,
                               updatedValuesJson: String, removedValuesJson: String) {
        println("Data changed in <$collectionName> in document <$documentID>")
    }

    override fun onDataRemoved(collectionName: String, documentID: String) {
        println("Data removed from <$collectionName> in document <$documentID>")

    }

    public override fun onDestroy() {
        mMeteor!!.disconnect()
        mMeteor!!.removeCallback(this)
        // or
        // mMeteor.removeCallbacks();

        // ...

        super.onDestroy()
    }


}
