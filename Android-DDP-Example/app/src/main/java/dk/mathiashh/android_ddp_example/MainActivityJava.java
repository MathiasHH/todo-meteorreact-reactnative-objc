package dk.mathiashh.android_ddp_example;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import im.delight.android.ddp.Meteor;
import im.delight.android.ddp.MeteorCallback;
import im.delight.android.ddp.ResultListener;

public class MainActivityJava extends AppCompatActivity implements MeteorCallback {

    private Meteor mMeteor;
    private ArrayList<TodoItem> list;
    private MyArrayAdapter arrayAdapter;
    private EditText input;

    class TodoItem {
        String id;
        boolean checked = false;
        boolean isPrivate;
        Date createdAt;
        String text;
    }

    private class MyArrayAdapter extends ArrayAdapter<TodoItem> {
        private final ArrayList<TodoItem> list;
        private View rowView;
        private LayoutInflater inflater;
        private ViewHolder viewHolder;

        public MyArrayAdapter(Context context, ArrayList<TodoItem> objects) {
            super(context, R.layout.todoitem, objects);
            list = objects;
        }

        @Override
        public View getView(int position, View cachedView, ViewGroup parent) {
            // Re-use the view and avoid inflating a layout for the current row.
            // This saves memory and CPU consumption.
            rowView = cachedView;

            if (rowView == null) {
                this.inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.todoitem, null);
                viewHolder = new ViewHolder();

                viewHolder.text = (TextView) rowView.findViewById(R.id.the_text);
                rowView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) rowView.getTag();
            }
            TodoItem todoItem = list.get(position);

            viewHolder.text.setText(todoItem.text);

            return rowView;
        }
    }

    //Is used to cache the TextView text See for example
    //http://developer.android.com/resources/samples/ApiDemos/src/com/example/android/apis/view/List14.html
    //The findViewById() method is an expensive operation, therefore we should avoid doing this operation if not necessary. You can use the "View Holder"" pattern for this.
    //A ViewHolder class is a static class in your adapter which allows to hold references to the fields in your layout so that you can avoid using findViewById() on your layout.
    //The ViewHolder stores a reference to the required views in a row. This ViewHolder is then attached to the row via the setTag() method. Every view can get a tag assigned.
    //If the row is recycled we can get the ViewHolder via getTag() method. This is much faster then the repetitive call of the findViewById() method.
    static class ViewHolder {
        public TextView text;
    }


    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(input.getWindowToken(), 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setupMeteor();

        input = (EditText) findViewById(R.id.editTextInput);
        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    Map<String, Object> insertValues = new HashMap<String, Object>();
                    String id = Meteor.uniqueID();
                    Date date = new Date();
                    insertValues.put("_id", id);
                    insertValues.put("createdAt", date);
                    insertValues.put("checked", false);
                    insertValues.put("isPrivate", false);
                    insertValues.put("text", input.getText().toString());
                    mMeteor.insert("tasks", insertValues);
                    input.setText("");
                    handled = true;

                    hideKeyboard();
                }
                return handled;
            }
        });

        final ListView listview = (ListView) findViewById(R.id.myListView);
        list = new ArrayList<>();
        arrayAdapter = new MyArrayAdapter(this, list);
        assert listview != null;
        listview.setAdapter(arrayAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                TodoItem todoItem = list.get(position);
                list.remove(position);
                mMeteor.remove("tasks", todoItem.id);
                arrayAdapter.notifyDataSetChanged();
            }
        });
    }

    private void setupMeteor() {
        // create a new instance
        mMeteor = new Meteor(this, "ws://139.59.159.223/websocket");
        // register the callback that will handle events and receive messages
        mMeteor.addCallback(this);
        // establish the connection
        mMeteor.connect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnect(boolean signedInAutomatically) {
        System.out.println("Connected");
        System.out.println("Is logged in: " + mMeteor.isLoggedIn());
        System.out.println("User ID: " + mMeteor.getUserId());

        if (signedInAutomatically) {
            System.out.println("Successfully logged in automatically");
        } else {
            // sign up for a new account
            mMeteor.registerAndLogin("john-doe", "john.doe@mathiashh.dk.", "kodeord",
                    new ResultListener() {

                        @Override
                        public void onSuccess(String result) {
                            System.out.println("Successfully registered: " + result);
                        }

                        @Override
                        public void onError(String error, String reason, String details) {
                            System.out.println("Could not register: " + error + " / " + reason + " / " + details);
                        }
                    });

            // sign in to the server
            mMeteor.loginWithUsername("john-doe", "kodeord", new ResultListener() {

                @Override
                public void onSuccess(String result) {
                    System.out.println("Successfully logged in: " + result);

                    System.out.println("Is logged in: " + mMeteor.isLoggedIn());
                    System.out.println("User ID: " + mMeteor.getUserId());
                }

                @Override
                public void onError(String error, String reason, String details) {
                    System.out.println("Could not log in: " + error + " / " + reason + " / " + details);
                }

            });

            // subscribe to data from the server
            String subscriptionId = mMeteor.subscribe("tasks");
        }
    }

    @Override
    public void onDisconnect() {
        System.out.println("Disconnected");
    }

    @Override
    public void onException(Exception e) {
        System.out.println("Exception");
        if (e != null) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDataAdded(String collectionName, String documentID, String newValuesJson) {
        System.out.println("Data changed in <" + collectionName + "> in document <" + documentID + ">");
        System.out.println("json : " + newValuesJson);
        TodoItem todoItem = new TodoItem();

        JSONObject json;
        try {
            json = new JSONObject(newValuesJson);
            try {
                String text = json.getString("text");
                if (text.length() > 0)
                {
                    todoItem.text = text;
                    todoItem.id = documentID;
//                                   todoItem.checked = json.getBoolean("checked");
//                todoItem.isPrivate = json.getBoolean("isPrivate");
                    list.add(todoItem);
                    arrayAdapter.notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDataChanged(String collectionName, String documentID,
                              String updatedValuesJson, String removedValuesJson) {
        System.out.println("Data changed in <" + collectionName + "> in document <" + documentID + ">");

        arrayAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDataRemoved(String collectionName, String documentID) {
        System.out.println("Data removed from <" + collectionName + "> in document <" + documentID + ">");
        for (int i = 0; i < list.size(); i++) {
            TodoItem t = list.get(i);
            if (t.id.equalsIgnoreCase(documentID)) {
                list.remove(i);
            }
        }
        arrayAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        mMeteor.disconnect();
        mMeteor.removeCallback(this);
        // or
        // mMeteor.removeCallbacks();

        // ...

        super.onDestroy();
    }


}
