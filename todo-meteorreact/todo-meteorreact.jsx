// Define a collection to hold our tasks
Tasks = new Mongo.Collection("tasks");

Meteor.methods({
    addTask(text) {
        // Make sure the user is logged in before inserting a task
        if (! Meteor.userId()) {
        //    throw new Meteor.Error("not-authorized");
        }
        return Tasks.insert({
            text: text,
            createdAt: new Date(),
            owner: Meteor.userId(),
            username: Meteor.user().username,
            private: true
        });
    },
    checkTask(taskId) {
        const task = Tasks.findOne(taskId);
        // Make sure only the task owner can make a task private
        if(task.owner !== Meteor.userId()){
            throw new Meteor.Error("not-authorized");
        }
    },
    checkTask2(taskId) {
        const task = Tasks.findOne(taskId);
        // If the task is private, make sure only the owner can delete it
        if(task.private && task.owner !== Meteor.userId()){
            throw new Meteor.Error("not-authorized");
        }
    },
    removeTask(taskId) {
        Meteor.call("checkTask2", taskId);
        Tasks.remove(taskId);
    },
    setChecked(taskId, setChecked) {
        Meteor.call("checkTask2", taskId);
        Tasks.update(taskId, { $set: { checked: setChecked} });
    },
    setPrivate(taskId, setToPrivate) {
        Meteor.call("checkTask", taskId);
        Tasks.update(taskId, { $set: { private: setToPrivate } });
    }
});
