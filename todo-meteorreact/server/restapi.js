// Login is here
//http://localhost:3000/api/login POST HEADERS(email:bob@mail.dk, password:kodeord)

// Global API configuration
var Api = new Restivus({
    defaultHeaders: {
        'Content-Type': 'application/json'
    },
    onLoggedIn: function () {
        console.log(this.userId + ') logged in');
    },
    onLoggedOut: function () {
        console.log(this.userId + ') logged out');
    },
    useDefaultAuth: true,
    prettyJson: true
});

// Generates: POST on /api/users and GET, DELETE /api/users/:id for
// Meteor.users collection
Api.addCollection(Meteor.users, {
    //excludedEndpoints: ['getAll', 'put'],
    routeOptions: {
        authRequired: true
    },
    endpoints: {
        post: {
            authRequired: true
        },
        delete: {
            roleRequired: 'admin'
        }
    }
});

// Generates: GET, POST on /api/tasks and GET, PUT, DELETE on
// /api/tasks/:id for the Tasks collection
//Api.addCollection(Tasks);
// CREATE, GETALL
Api.addRoute('tasks/',
    { authRequired: true},
    {
        post: function () {
            const bodyP = this.bodyParams;
            const text = bodyP.text;
            const date = new Date();
            var taskId;
            const userId = this.userId;
            const username = this.user.username;

            if (taskId = Tasks.insert({
                    text: text,
                    createdAt: date,
                    owner: userId,
                    username: username,
                    private: true
                }))
            {
                return { "status": "success",
                    "data": [
                        {
                            "_id" : taskId,
                            "createdAt" : date,
                            "owner" : userId,
                            "username" : username,
                            "private" : 1,
                            "text" : text
                        }
                    ]};
            }
            return {status: 'fail', data: {message: 'Todo NOT inserted'}};
            // todo use a meteor method instead. create a method that will work for this rest api instead of this one
            // return Meteor.call("addTask", text);
        },
        get: function () {
            const tasks = Tasks.find({}).fetch();
            return tasks;
        }
    });

// READ, UPDATE, DELETE
//Api.addRoute('articles/:id', {authRequired: true}, {
//    get: function () {
//        //object id
//        //const oid = new Mongo.ObjectID(this.urlParams.id); // http://stackoverflow.com/questions/24516110/meteor-find-a-document-from-collection-via-mongo-objectid
//        //https://groups.google.com/forum/#!topic/meteor-talk/f-ljBdZOw
//        // string id
//        const oid = this.urlParams.id;
//        return Articles.findOne(oid);
//    },
//    put: function () {
//        const bodyP = this.bodyParams;
//        const theAuthor = bodyP.author;
//        const theTitle = bodyP.title;
//        //const oid = new Mongo.ObjectID(this.urlParams.id);
//        const oid = this.urlParams.id;
//        Articles.update({_id :oid }, { $set: { title: theTitle, author: theAuthor} });
//        return Articles.findOne(oid);
//    },
//    delete: function(){
//        //if (Articles.remove(new Mongo.ObjectID(this.urlParams.id))) {
//        if (Articles.remove(this.urlParams.id)) {
//            return {status: 'success', data: {message: 'Article removed'}};
//        }
//        return {
//            statusCode: 404,
//            body: {status: 'fail', message: 'Article not found'}
//        };
//    },
//delete: {
//    roleRequired: ['author'/*, 'admin'*/], // we currently have no roles setup
//    action: function () {
//        const oid = new Mongo.ObjectID(this.urlParams.id);
//        if (Articles.remove(oid)) {
//            return {status: 'success', data: {message: 'Article removed'}};
//        }
//        return {
//            statusCode: 404,
//            body: {status: 'fail', message: 'Article not found'}
//        };
//    }
//}
//});