#import "TodoObjCSetupDependencies.h"
#import "TodoObjCCommunicator.h"

@implementation TodoObjCSetupDependencies { UIStoryboard *_storyboard; }

-(instancetype) init {
	self = [super init];
	if (self) {
		_storyboard =  [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	}
	return self;
}

-(LoginViewController *) loginViewController {
	
	return [_storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
}

-(TodoViewController2 *) todoViewController2 {
	
	TodoViewController2 *todoVC = [_storyboard instantiateViewControllerWithIdentifier:@"TodoViewController2"];  // [TodoViewController2 new];
	//inject dependecies
	todoVC.manager = [self todoObjcManager];
	return todoVC;
}

-(TodoObjCManager *) todoObjcManager {
	TodoObjCManager *manager = [TodoObjCManager new];
	manager.communicator = [TodoObjCCommunicator new];
	manager.communicator.delegate = manager;
	
	return manager;
}

@end
