//
//  LoginViewController.m
//  todo-ObjC
//
//  Created by Mathias Hansen on 19/02/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "LoginViewController.h"
#import "Network.h"
#import "LoginRules.h"
#import "MyMacros.h"
#import "TodoViewController2.h"
#import "TodoObjCManager.h"
#import "TodoObjCSetupDependencies.h"

@interface LoginViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *logInButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation LoginViewController {
	BOOL _isLoading;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.

	// round button - http://stackoverflow.com/questions/5047818/how-to-round-the-corners-of-a-button
	self.logInButton.layer.cornerRadius = 3;
	self.logInButton.clipsToBounds = YES;
	self.errorLabel.layer.cornerRadius = 3;
	self.errorLabel.clipsToBounds = YES;
	self.errorLabel.text = @"";

	self.emailTextField.delegate = self;
	self.passwordTextField.delegate = self;
	self.passwordTextField.secureTextEntry = YES;


}

-(void)errorMessage:(NSString *)message {
	self.errorLabel.text = message;
}

- (void)whichVCToShow {
	UIAlertController * alert = [UIAlertController
								 alertControllerWithTitle:@""
								 message:@"Show ViewController 1 or 2?"
								 preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* ok = [UIAlertAction
						 actionWithTitle:@"1"
						 style:UIAlertActionStyleDefault
						 handler:^(UIAlertAction * action)
						 {
							 [self showTheVC:1];
							 [alert dismissViewControllerAnimated:YES completion:nil];
							 
						 }];
	UIAlertAction* cancel = [UIAlertAction
							 actionWithTitle:@"2"
							 style:UIAlertActionStyleDefault
							 handler:^(UIAlertAction * action)
							 {
								 [self showTheVC:2];
								 [alert dismissViewControllerAnimated:YES completion:nil];
							 }];
	
	[alert addAction:ok];
	[alert addAction:cancel];
	
	[self presentViewController:alert animated:YES completion:nil];
}

-(void)showTheVC:(int)which {
	// todo strings should be in strings file
	NSString *viewControllerString = which == 1 ? @"showTodoViewController" : @"showTodoViewController2";
	NSString *loggedInVC = which == 1 ? @"TodoViewController" : @"TodoViewController2";
		NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	[userDefaults setObject:loggedInVC forKey:WHICH_VC];
	
	[self performSegueWithIdentifier: viewControllerString sender: nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	if ([segue.identifier isEqualToString:@"showTodoViewController2"]) {
//		TodoViewController2 *todoVC2 = segue.destinationViewController;
		TodoViewController2 *todoVC2 = [[TodoObjCSetupDependencies new] todoViewController2];
	}
}

- (IBAction)login {

	
	[self.passwordTextField endEditing:YES];
	[self.emailTextField endEditing:YES];

	NSString *username = self.emailTextField.text;
	NSString *password = self.passwordTextField.text;
	LoginRules *loginRules = [LoginRules new];
	if( ![loginRules isValidEmail:username] || ![loginRules passwordPolicy:password]) {
		[self errorMessage:NSLocalizedString(@"Wrong Email or Password", nil)];
		return;
	}
	
	[self.spinner startAnimating];
	_isLoading = YES;
	NSDictionary *parameters = @{@"username":username,@"password":password};

	[[Network new] login:parameters withCallback:^(NSString *error, id responseObject) {
		[self.spinner stopAnimating];
		_isLoading = NO;
//		if(2==1) {
		if(error) {
			[self errorMessage:error];
		}else {
			id data = [responseObject objectForKey:DATA_KEY];
			id userId = [data objectForKey:USER_ID_KEY];
			id authToken = [data objectForKey:AUTH_TOKEN__KEY];
			// todo maybe use realmio for for login functionality
			[[NSUserDefaults standardUserDefaults] setObject:userId forKey:USER_ID_KEY];
			[[NSUserDefaults standardUserDefaults] setObject:authToken forKey:AUTH_TOKEN__KEY];
			[[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGGED_IN];
			
			[self whichVCToShow];
		}
	}];
}

// if user is typing we dont want and error message - http://stackoverflow.com/questions/5190097/is-it-possible-to-know-if-user-is-typing-or-deleting-characters-in-a-textfield
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//	// if there is an error message
//	if (self.errorLabel.text.length > 0) {
//		self.errorLabel.text = @"";
//	}
//
//	return YES;
//}

// if user is touching a textfield to start typing we dont want and error message
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

	// if there is an error message
	if (self.errorLabel.text.length > 0) {
		self.errorLabel.text = @"";
	}

	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	// when clicking done on the keyboard after clicking on password we want to login
	if(textField.tag == 2) {
		[self login];
	}
	// hide keyboard when clicking done on the keyboard
	[textField resignFirstResponder];
	return NO;
}

// hide keyboard when clicking outside textfield
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	[self.view endEditing:YES];
}



- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
