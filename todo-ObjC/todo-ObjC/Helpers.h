//
// Created by Mathias Hansen on 28/02/16.
// Copyright (c) 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <Foundation/Foundation.h>


@class UIViewController;

@class UIAlertController;

@interface Helpers : NSObject

+ (void)gotoViewController:(NSString *)gotoVC fromViewController:(UIViewController *)fromVC block:(void (^)(void))block;

+(void)showUIAlertController:(NSString *)title message:(NSString *)message withCallback:(void (^)(UIAlertController *))callback;
@end