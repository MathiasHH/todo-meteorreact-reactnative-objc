//
//  MyLabel.h
//  todo-ObjC
//
//  Created by Mathias Hansen on 24/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyLabel : UILabel

- (instancetype)initWithX:(CGFloat)x y:(CGFloat)y;

- (CGSize)intrinsicContentSize;

@end


