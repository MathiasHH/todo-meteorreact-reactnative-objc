//
//  Network.m
//  todo-ObjC
//
//  Created by Mathias Hansen on 23/02/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "Network.h"
#import "MyMacros.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>

@interface Network ()
@property(nonatomic, strong) AFHTTPSessionManager *manager;
@end

@implementation Network

-(AFHTTPSessionManager *)manager {
	if(_manager == nil) {
		_manager = [AFHTTPSessionManager manager];
		AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer new];
		[requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID_KEY] forHTTPHeaderField:X_USER_ID];
		[requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:AUTH_TOKEN__KEY] forHTTPHeaderField:X_AUTH_TOKEN];
		_manager.requestSerializer = requestSerializer;

		_manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
	}
	return _manager;
}

// this wouldnt cancel the request
//-(void)abort {
////	[self.manager.operationQueue cancelAllOperations];
//	[self.manager invalidateSessionCancelingTasks:YES];
//}

-(void)addTodo:(NSString *)theTodo withCallback:(void (^)(NSError *, id))callback {

	NSDictionary *parameters = @{@"text":theTodo};

	NSString *url = [NSString stringWithFormat:@"%@%@%@", PROTOCOL, HOSTNAME, PARAMETER_1];

	[self.manager POST:url
			parameters:parameters
			  progress:nil
			   success:^(NSURLSessionTask *task, id responseObject) {
				   NSLog(@"Success: %@", responseObject);
				   callback(nil, responseObject);
			   } failure:^(NSURLSessionTask *operation, NSError *error) {
				NSLog(@"Error: %@", error);
				callback(error, nil);
			}];
}

-(void)get:(void (^) (id, id))callback
{
	NSDictionary *parameters = @{};
	NSString *parameter_2 = nil;// @"ut4iiLiX5tMPwfa4C";
	NSString *parameter2 = parameter_2 != nil ? parameter_2 : @"";

	NSString *url = [NSString stringWithFormat:@"%@%@%@%@", PROTOCOL, HOSTNAME, PARAMETER_1, parameter2];

	[self.manager GET:url parameters:parameters progress:nil
			  success:^(NSURLSessionTask *task,
id responseObject) {
				  NSLog(@"JSON: %@", responseObject);
				  callback(nil, responseObject);

			  } failure:^(NSURLSessionTask *operation, NSError *error) {
				NSLog(@"Error: %@", error);
				callback(error, nil);
			}];
}

-(void)delete:(NSString *)objectToDelete {

	NSDictionary *parameters = @{};
	NSString *url = [NSString stringWithFormat:@"%@%@%@%@", PROTOCOL, HOSTNAME, PARAMETER_1, objectToDelete];

//	NSDictionary *parameters = @{X_USER_ID:[[NSUserDefaults standardUserDefaults] objectForKey:USER_ID_KEY], X_AUTH_TOKEN:[[NSUserDefaults standardUserDefaults] objectForKey:AUTH_TOKEN__KEY]};

	[self.manager DELETE:url parameters:parameters success:^(NSURLSessionTask *task, id responseObject) {
		NSLog(@"Success: %@", responseObject);
		//		callback(nil, responseObject);
	} failure:^(NSURLSessionTask *operation, NSError *error) {
		NSLog(@"Error: %@", error);
		//		callback(error.userInfo[@"NSLocalizedDescription"], nil);
	}];
}

-(void)login:(NSDictionary *)usernameandpassword withCallback:(void (^)(id, id))callback {

#if FAKE_SERVER == 1
	/* Debug only code */
	NSString *userName = usernameandpassword[@"username"];
	NSString *password = usernameandpassword[@"password"];
	if([userName isEqualToString:@"bob@mail.dk"] && [password isEqualToString:@"kodeord"])
		callback(nil, @{@"data" :
				@{@"authToken" : @"hiKUL6LLXyeSdLRzXI8HxR6972f6zm8Qjb2ioaxQ6Wi",
						@"userId" : @"DZekBStbiw8PWpSoi",
				},
				@"status" : @"success",
		});
	else
		callback(NSLocalizedString(@"Error :(", nil), nil);
#else
	//	NSDictionary *parameters = @{@"username":@"bob@mail.dk",@"password":@"kodeord"};


	NSString *url = [NSString stringWithFormat:@"%@%@%@", PROTOCOL, HOSTNAME, LOGIN_PARAMETER];
	[self.manager POST:url
			parameters:usernameandpassword
			  progress:nil
			   success:^(NSURLSessionTask *task, id responseObject) {
				   NSLog(@"Success: %@", responseObject);
				   callback(nil, responseObject);
			   } failure:^(NSURLSessionTask *operation, NSError *error) {
				NSLog(@"Error: %@", error);
				callback(error.userInfo[@"NSLocalizedDescription"], nil);
			}];
#endif
}

@end
