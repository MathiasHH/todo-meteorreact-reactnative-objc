//
//  LoginRules.h
//  FunnyTelco
//
//  Created by Mathias Hansen on 05/03/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginRules : NSObject
- (BOOL)isValidEmail:(NSString *)email;

/* Todo until it has been discussed what the policy is for a password it is just set to be 3 characters long*/
-(BOOL)passwordPolicy:(NSString *)password;

@end
