//
// Created by Mathias Hansen on 10/03/16.
// Copyright (c) 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const PROTOCOL;
extern NSString * const HOSTNAME;

@interface Constants : NSObject
@end