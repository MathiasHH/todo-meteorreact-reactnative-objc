//
//  MyMacros.h
//  FunnyTelco
//
//  Created by Mathias Hansen on 01/03/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#ifndef MyMacros_h
#define MyMacros_h

/* If we have no server */
#define FAKE_SERVER 0

#define DATA_KEY @"data"
#define USER_ID_KEY @"userId"
#define AUTH_TOKEN__KEY @"authToken"
#define IS_LOGGED_IN @"isLoggedIn"
#define WHICH_VC @"whichVC"


#define X_USER_ID @"X-User-Id"
#define X_AUTH_TOKEN @"X-Auth-Token"


#define PARAMETER_1 @"api/tasks/"
#define LOGIN_PARAMETER @"api/login/"

#endif /* MyMacros_h */
