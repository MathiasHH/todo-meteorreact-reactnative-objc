//
// Created by Mathias Hansen on 10/03/16.
// Copyright (c) 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Todo : NSObject
@property (nonatomic, strong) NSString *owner;
@property (nonatomic, strong) NSString *_id;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, assign) BOOL private;
@property (nonatomic, strong) NSString *createdAt;
@end