//
// Created by Mathias Hansen on 28/02/16.
// Copyright (c) 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "Helpers.h"
#import "LoginViewController.h"


@implementation Helpers

+ (void)gotoViewController:(NSString *)gotoVC fromViewController:(UIViewController *)fromVC block:(void (^)(void))block
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *gotoViewController = [storyboard instantiateViewControllerWithIdentifier:gotoVC];
    [fromVC presentViewController:gotoViewController animated:YES completion:^{
        block();
    }];
}

+ (void)showUIAlertController:(NSString *)title message:(NSString *)message withCallback:(void(^)(UIAlertController *))callback {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    callback(alertController);
}

@end