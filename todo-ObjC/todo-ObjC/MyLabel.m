//
//  MyLabel.m
//  todo-ObjC
//
//  Created by Mathias Hansen on 24/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "MyLabel.h"

@implementation MyLabel { CGFloat _x, _y; }

- (instancetype)initWithX:(CGFloat)x y:(CGFloat)y
{
	_x = x;
	_y = y;
	return [self init];
}

- (instancetype)init
{
	self = [super init];
	if (self)
	{
		self.textAlignment = NSTextAlignmentLeft;
		self.textColor = [UIColor whiteColor];
	}
	return self;
}

- (CGSize)intrinsicContentSize
{
	return CGSizeMake(_x != 0 ? _x : 100, _y != 0 ? _y : 100);
}
@end