//
// Created by Mathias Hansen on 10/03/16.
// Copyright (c) 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "Constants.h"

NSString * const PROTOCOL = @"http://";
NSString * const HOSTNAME = @"localhost:3000/";
//NSString * const HOSTNAME = @"192.168.87.104:3000/";

@implementation Constants
@end