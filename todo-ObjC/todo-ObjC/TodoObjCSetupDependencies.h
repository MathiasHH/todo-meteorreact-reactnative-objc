//
//  TodoObjCSetupDependencies.h
//  todo-ObjC
//
//  Created by Mathias Hansen on 24/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginViewController.h"
#import "TodoViewController2.h"

@interface TodoObjCSetupDependencies : NSObject

-(LoginViewController *) loginViewController;
-(TodoViewController2 *) todoViewController2;
-(TodoObjCManager *) todoObjcManager;
@end
