//
//  DataBuilder.m
//  todo-ObjC
//
//  Created by Mathias Hansen on 24/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "DataBuilder.h"
#import "Todo.h"

@implementation DataBuilder

// todo move to new file
- (NSArray<Todo*>*)parseDictionary:(NSDictionary *)dictionary
{
	//	NSArray *array = dictionary[DATA_KEY];
	//	if (array == nil) {
	//		NSLog(@"Expected 'results' array");
	//		return;
	//	}

	NSMutableArray<Todo*>  *items = [NSMutableArray new];
									 
	// get all from server and add them to our list
	//	for (NSDictionary *resultDict in array) {
	for (NSDictionary *resultDict in dictionary) {
		if(resultDict) {
			Todo *todo = [Todo new];
			todo.owner = resultDict[@"_owner"];
			todo._id = resultDict[@"_id"];
			todo.text = resultDict[@"text"];
			todo.username = resultDict[@"username"];
			todo.private = [resultDict[@"checked"] boolValue];
			todo.createdAt = resultDict[@"createdAt"];
			[items addObject:todo];
		}
	}
	
	// we only want unique
	NSDictionary *tempDict = [NSDictionary dictionaryWithObjects:items forKeys:[items valueForKey:@"_id"]];
	
	NSMutableArray  *temp  = [NSMutableArray new];
	for (id key in tempDict){
		[temp addObject:tempDict[key]];
	}
	
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdAt" ascending:YES];
	NSArray *sortDescriptors = @[sortDescriptor];
	NSArray * sorted = [temp sortedArrayUsingDescriptors:sortDescriptors];
	items = nil;
	items = [NSMutableArray new];
	items = [NSMutableArray arrayWithArray:sorted];
	
	return items;
}

@end
