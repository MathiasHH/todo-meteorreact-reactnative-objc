//
//  ITodoObcCManager.h
//  todo-ObjC
//
//  Created by Mathias Hansen on 24/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ITodoObcCManager <NSObject>
@property (weak, nonatomic) id <TodoObjCManagerDelegate> delegate;
@end
