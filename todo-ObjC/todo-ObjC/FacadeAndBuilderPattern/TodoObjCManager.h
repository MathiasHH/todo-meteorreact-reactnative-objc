//
//  TodoObjCManager.h
//  todo-ObjC
//
//  Created by Mathias Hansen on 24/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TodoObjCManagerDelegate.h"
#import "TodoObjCCommunicatorDelegate.h"
#import "TodoObjCCommunicator.h"
#import "ITodoObcCManager.h"
/**
 * A façade providing access to the Backend service.
 * Application code should only use this class to get at Data.
 */

@interface TodoObjCManager : NSObject <TodoObjCCommunicatorDelegate>

-(void)getData;

@property (weak, nonatomic) id <TodoObjCManagerDelegate> delegate;
@property (strong) TodoObjCCommunicator  *communicator;
@end
