//
//  TodoObjCCommunicator.h
//  todo-ObjC
//
//  Created by Mathias Hansen on 24/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TodoObjCCommunicatorDelegate.h"

@interface TodoObjCCommunicator : NSObject

@property (weak) id <TodoObjCCommunicatorDelegate> delegate;
- (void)getData:(BOOL)showSpinner;
@end
