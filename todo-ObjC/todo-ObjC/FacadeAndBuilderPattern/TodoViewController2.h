//
//  TodoViewController2.h
//  todo-ObjC
//
//  Created by Mathias Hansen on 24/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "TodoObjCManagerDelegate.h"
#import "TodoObjCManager.h"

@interface TodoViewController2 : UIViewController <TodoObjCManagerDelegate>
//@property (weak, nonatomic) id <TodoObjCManagerDelegate> manager;
@property (strong, nonatomic) TodoObjCManager *manager;


@end
