#import <Foundation/Foundation.h>

@protocol TodoObjCManagerDelegate <NSObject>

//@property (weak, nonatomic) id <TodoObjCManagerDelegate> delegate;

/**
 * The manager retrieved a list of data from Backend.
 */
- (void)didReceiveData: (NSArray *)data;

/**
 * todo write something here
 */
//- (void)getData;

@end
