//
//  TodoObjCCommunicator.m
//  todo-ObjC
//
//  Created by Mathias Hansen on 24/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "TodoObjCCommunicator.h"
#import "Helpers.h"
#import "Network.h"

@implementation TodoObjCCommunicator

//- (void)searchForQuestionsWithTag:(NSString *)tag {
//	[self fetchContentAtURL: [NSURL URLWithString:
//							  [NSString stringWithFormat: @"http://api.stackoverflow.com/1.1/search?tagged=%@&pagesize=20", tag]]
//			   errorHandler: ^(NSError *error) {
////				   [self.delegate searchingForQuestionsFailedWithError: error];
//			   }
//			 successHandler: ^(NSString *objectNotation) {
//				 [self.delegate receivedDataJSON: objectNotation];
//			 }];
//}

- (void)fetchContentAtURL:(NSURL *)url
			 errorHandler:(void (^)(NSError *))errorBlock
		   successHandler:(void (^)(NSString *))successBlock
{
//	fetchingURL = url;
//	errorHandler = [errorBlock copy];
//	successHandler = [successBlock copy];
//	NSURLRequest *request = [NSURLRequest requestWithURL: fetchingURL];
//	
//	[self launchConnectionForRequest: request];
	
	
}


- (void)getData:(BOOL)showSpinner
{
	[[Network new] get:^(NSError * error, id responseObject) {
		
		if(error) {
			[Helpers showUIAlertController:[NSString stringWithFormat:@"Error code: %ld",(long)error.code] message:error.localizedDescription withCallback:^(UIAlertController * alertController) {
//				[self presentViewController:alertController animated:YES completion:nil];
			}];
		}
//		else if(cancelGetData) {
//			cancelGetData = NO; // we cancel the get data and now we reset so we can get data again.
			
//		}
		else {
//			[self parseDictionary:responseObject];
//			[self setTodoListLabel];
//			[tableView reloadData];

			[self.delegate receivedDataJSON:responseObject];
		}
		
//		if(showSpinner)
//			[refreshControl endRefreshing];
		
	}];
}

@end
