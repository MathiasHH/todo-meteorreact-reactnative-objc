//
//  TodoObjCCommunicatorDelegate.h
//  todo-ObjC
//
//  Created by Mathias Hansen on 24/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TodoObjCCommunicatorDelegate <NSObject>

/**
 * The communicator received a response from the backend
 */
- (void)receivedDataJSON: (id)objectNotation;

@end
