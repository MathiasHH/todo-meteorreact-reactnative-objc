//
//  TodoViewController2.m
//  todo-ObjC
//
//  Created by Mathias Hansen on 24/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//


#import "TodoViewController2.h"
#import "Utility.h"
#import "Helpers.h"
#import "Network.h"
#import "MyMacros.h"
#import "Todo.h"
#import "MyLabel.h"
#import "TodoObjCSetupDependencies.h"

#pragma mark - TodoViewController2
@interface TodoViewController2 () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSNumber *contentOffsetAtStartOfDragging;
@end

@implementation TodoViewController2 {
	UIView *containerView;
	UITextField *createTodoTF;
	MyLabel *headerLabel, *loginLabel;
	UIButton *logoutButton;
	UITableView *tableView;
	NSMutableArray<Todo*>  *items;
	UIRefreshControl *refreshControl;
	BOOL cancelGetData;
}


- (void)viewDidLoad {
	[super viewDidLoad];
//	[self getData:NO];

	
	// todo fix that manager is nill when comming from logout -> login
	if (!self.manager)
		self.manager = [[TodoObjCSetupDependencies new] todoObjcManager];
	
	self.manager.delegate = self;
	[self.manager getData];
	
}

#pragma mark - TodoObjCManagerDelegate start
-(void)didReceiveData:(NSMutableArray<Todo*> *)data {
	items =  data;
	[self setTodoListLabel];
	[tableView reloadData];
}

#pragma mark - Other stuff
- (void)getData:(BOOL)showSpinner
{
	[[Network new] get:^(NSError * error, id responseObject) {
		
		if(error) {
			[Helpers showUIAlertController:[NSString stringWithFormat:@"Error code: %ld",(long)error.code] message:error.localizedDescription withCallback:^(UIAlertController * alertController) {
				[self presentViewController:alertController animated:YES completion:nil];
			}];
		}
		else if(cancelGetData) {
			cancelGetData = NO; // we cancel the get data and now we reset so we can get data again.
			
		}
		else {
			[self parseDictionary:responseObject];
			[self setTodoListLabel];
			[tableView reloadData];
		}
		
		if(showSpinner)
			[refreshControl endRefreshing];
		
	}];
}


- (void)showSpinnerAndGetData
{
	if (refreshControl) {
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"MMM d, h:mm a"];
		NSString *title = [NSString stringWithFormat:@"Last update: %@\t%@",[formatter stringFromDate:[NSDate date]], @"Drag up to cancel"];
		NSDictionary *attrsDictionary = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
		NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
		refreshControl.attributedTitle = attributedTitle;
		
		[self getData:YES];
	}
}

- (void)parseDictionary:(NSDictionary *)dictionary
{
	//	NSArray *array = dictionary[DATA_KEY];
	//	if (array == nil) {
	//		NSLog(@"Expected 'results' array");
	//		return;
	//	}
	
	// get all from server and add them to our list
	//	for (NSDictionary *resultDict in array) {
	for (NSDictionary *resultDict in dictionary) {
		if(resultDict) {
			Todo *todo = [Todo new];
			todo.owner = resultDict[@"_owner"];
			todo._id = resultDict[@"_id"];
			todo.text = resultDict[@"text"];
			todo.username = resultDict[@"username"];
			todo.private = [resultDict[@"checked"] boolValue];
			todo.createdAt = resultDict[@"createdAt"];
			[items addObject:todo];
		}
	}
	
	// we only want unique
	NSDictionary *tempDict = [NSDictionary dictionaryWithObjects:items forKeys:[items valueForKey:@"_id"]];
	
	NSMutableArray  *temp  = [NSMutableArray new];
	for (id key in tempDict){
		[temp addObject:tempDict[key]];
	}
	
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdAt" ascending:YES];
	NSArray *sortDescriptors = @[sortDescriptor];
	NSArray * sorted = [temp sortedArrayUsingDescriptors:sortDescriptors];
	items = nil;
	items = [NSMutableArray new];
	items = [NSMutableArray arrayWithArray:sorted];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}

- (void)loadView
{
	[super loadView];
	
	//container blue/green
	[self createContainerView];
	[self setupConstraintsForContainer];
	[self createLabelsAndButton];
	[self setupConstraintsForLabelsAndButtons];
	
	// add todos
	[self createTodoTF];
	[self setupConstraintsForCreateTodoTF];
	
	// add tableview
	[self createTableView];
	[self setupConstraintsForTableView];
	[self createRefreshControl];
	
	// setup constraints for the containers
	[self createConstrainsContainers];
	
	[self.view layoutIfNeeded];
}

- (void)createRefreshControl
{
	// Initialize the refresh control.
	refreshControl = [[UIRefreshControl alloc] init];
	refreshControl.backgroundColor = [UIColor purpleColor];
	refreshControl.tintColor = [UIColor whiteColor];
	[refreshControl addTarget:self action:@selector(showSpinnerAndGetData) forControlEvents:UIControlEventValueChanged];
	[tableView addSubview:refreshControl];
}

// http://stackoverflow.com/questions/23794957/ios-stop-uirefreshcontrol-on-push-up
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	if ([refreshControl isRefreshing]) {
		self.contentOffsetAtStartOfDragging = @(scrollView.contentOffset.y);
	}
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if ([refreshControl isRefreshing] &&
		scrollView.contentOffset.y > [self.contentOffsetAtStartOfDragging floatValue])
	{
		[refreshControl endRefreshing];
		cancelGetData = YES; // when we scroll up we "cancel", dont want to use the data getting data
	}
	
	self.contentOffsetAtStartOfDragging = nil;
}

- (void)createLabelsAndButton
{
	headerLabel = [[MyLabel alloc] initWithX:80 y:80];
	[self setTodoListLabel];
	//	headerLabel.backgroundColor = [UIColor colorWithRed:0.694 green:0.894 blue:0.702 alpha:1];
	[headerLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
	[containerView addSubview:headerLabel];
	
	loginLabel = [[MyLabel alloc] initWithX:80 y:80];
	loginLabel.text = [NSString stringWithFormat:@"The logged in user is: %@",[[NSUserDefaults standardUserDefaults]
																			   objectForKey:USER_ID_KEY]];
	loginLabel.numberOfLines = 0;
	[loginLabel setFont:[loginLabel.font fontWithSize:10.0f]];
	//	headerLabel.backgroundColor = [UIColor colorWithRed:0.694 green:0.894 blue:0.702 alpha:1];
	[loginLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
	[containerView addSubview:loginLabel];
	
	logoutButton = [UIButton new];
	
	[logoutButton setTitle:@"Logout" forState:UIControlStateNormal];
	logoutButton.titleLabel.font = [UIFont systemFontOfSize:8.0f];
	logoutButton.titleLabel.textColor = [UIColor darkGrayColor];
	logoutButton.backgroundColor = [UIColor colorWithRed:0.0 green:0.702 blue:0.5 alpha:1];
	logoutButton.layer.cornerRadius = 10;
	logoutButton.clipsToBounds = YES;
	[logoutButton setTranslatesAutoresizingMaskIntoConstraints:NO];
	[logoutButton addTarget:self action:@selector(logoutClick) forControlEvents:UIControlEventTouchUpInside];
	[containerView addSubview:logoutButton];
	
	
	//	[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
}



//- (void)deviceOrientationDidChange:(id)deviceOrientationDidChange;
//{
//	//Obtain current device orientation
//	UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
//
//	NSLog(@"orientation : %d", orientation);
//	[logoutButton setHidden:!(orientation == 1 || orientation == 5 || orientation == 2)];
//}
//
//- (void)dealloc;
//{
//	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
//}

#pragma mark create todo
- (void)createTodoTF
{
	createTodoTF = [UITextField new];
	//	createTodoTF.layoutMargins = 10.0f;
	createTodoTF.layer.sublayerTransform = CATransform3DMakeTranslation(20, 0, 0); // move text to the right;
	createTodoTF.placeholder = @"\tType to add new todo";
	createTodoTF.tag = 1;
	createTodoTF.delegate = self;
	//	createTodoTF.backgroundColor = [UIColor redColor];
	createTodoTF.backgroundColor = [UIColor colorWithRed:0.694 green:0.894 blue:0.702 alpha:0.5];
	[createTodoTF setTranslatesAutoresizingMaskIntoConstraints:NO];
	[createTodoTF addTarget:self action:@selector(addTodo) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:createTodoTF];
	
}

- (void)createContainerView
{
	containerView = [UIView new];
//	containerView.backgroundColor = [UIColor colorWithRed:0.0 green:0.702 blue:0.698 alpha:1];
	containerView.backgroundColor = [UIColor colorWithRed:0.694 green:0.294 blue:0.702 alpha:0.5];
	[containerView setTranslatesAutoresizingMaskIntoConstraints:NO];
	[self.view addSubview:containerView];
}

- (void)addTodo
{
	NSString *todoText = createTodoTF.text;
	[[Network new] addTodo:todoText withCallback:^(NSError * error, id responseObject){
		if(error) {
			[Helpers showUIAlertController:[NSString stringWithFormat:@"Error code: %ld",(long)error.code] message:error.localizedDescription withCallback:^(UIAlertController * alertController) {
				[self presentViewController:alertController animated:YES completion:nil];
			}];
		}
		else {
			Todo *todo = [Todo new];
			id data = responseObject[DATA_KEY][0];
			// todo move strings to constants
			todo.owner = data[@"owner"];
			todo._id = data[@"_id"];
			todo.text = data[@"text"];
			todo.username = data[@"username"];
			todo.private = [data[@"private"] boolValue];
			todo.createdAt = data[@"createdAt"];
			
			[items addObject:todo];
			[self setTodoListLabel];
			[tableView reloadData];
		}
	}];
}

- (void)setTodoListLabel
{
	headerLabel.text = [NSString stringWithFormat:@"Todo List (%lu)", (unsigned long)items.count];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	// when clicking done on the keyboard after clicking on password we want to login
	if(textField.tag == 1) {
		[self addTodo];
	}
	textField.text = @"";
	// hide keyboard when clicking done on the keyboard
	[textField resignFirstResponder];
	return NO;
}

#pragma mark tableviw
#pragma mark tableview - read todos
- (void)createTableView
{
	tableView = [[UITableView alloc] initWithFrame:CGRectZero
											 style:UITableViewStylePlain];
	tableView.translatesAutoresizingMaskIntoConstraints = NO;
	tableView.delegate = self;
	tableView.dataSource = self;
	
	[tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
	//	items = [@"Alpha Bravo Charlie Delta Echo Foxtrot Golf Hotel India Juliet Kilo Lima Mike November Oscar Papa Romeo Quebec Sierra Tango Uniform Victor Whiskey Xray Yankee Zulu" componentsSeparatedByString:@" "];
	items = [NSMutableArray arrayWithCapacity:10];
	[self.view addSubview:tableView];
}



// Number of sections
- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView
{
	// Return the number of sections.
	if (items) {
		tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
		return 1;
	} else {
		// Display a message when the table is empty
		UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
		
		messageLabel.text = @"No data is currently available. Please pull down to refresh.";
		messageLabel.textColor = [UIColor blackColor];
		messageLabel.numberOfLines = 0;
		messageLabel.textAlignment = NSTextAlignmentCenter;
		messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
		[messageLabel sizeToFit];
		
		tableView.backgroundView = messageLabel;
		tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	}
	return 0;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section;
{
	return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
	// Cell label
	Todo *todo = items[(NSUInteger) indexPath.row];
	cell.textLabel.text = todo.text;
	return cell;
}

#pragma mark tableview - delete todo
- (BOOL)tableView:(UITableView *)aTableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		//remove the deleted object from your data source.
		//If your data source is an NSMutableArray, do
		NSUInteger index = (NSUInteger) indexPath.row;
		Todo *todo = items[index];
		[items removeObjectAtIndex:index];
		[[Network new] delete:todo._id];
		[self setTodoListLabel];
		[tableView reloadData]; // tell table to refresh now
	}
}

// hide keyboard when clicking outside textfield
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	[self.view endEditing:YES];
}



#pragma mark -
- (void)logoutClick
{
	//	[[Network new] get];
	[Helpers gotoViewController:@"LoginViewController" fromViewController:self block:^{
		// logout. Now we just set this to false. In production we would call the server to logout to destroy the token.
		[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isLoggedIn"];
	}];
}


#pragma mark - AutoLayout Constraints

- (void)setupConstraintsForLabelsAndButtons
{
	// Width constraint
	[containerView addConstraint:[NSLayoutConstraint constraintWithItem:headerLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:containerView attribute:NSLayoutAttributeWidth multiplier:0.33 constant:0]];
	// Height constraint
	[containerView addConstraint:[NSLayoutConstraint constraintWithItem:headerLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:containerView attribute:NSLayoutAttributeHeight multiplier:0.33 constant:0]];
	// Width constraint
	[containerView addConstraint:[NSLayoutConstraint constraintWithItem:loginLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:containerView attribute:NSLayoutAttributeWidth multiplier:0.33 constant:0]];
	// Height constraint
	[containerView addConstraint:[NSLayoutConstraint constraintWithItem:loginLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:containerView attribute:NSLayoutAttributeHeight multiplier:0.33 constant:0]];
	
	[containerView addConstraints:CONSTRAINTS(@"H:|-8-[headerLabel]", NSDictionaryOfVariableBindings(headerLabel))];
	[containerView addConstraints:CONSTRAINTS(@"H:[loginLabel]|", NSDictionaryOfVariableBindings(loginLabel))];
	[containerView addConstraints:CONSTRAINTS(@"V:|-20-[headerLabel]", NSDictionaryOfVariableBindings(headerLabel, loginLabel, logoutButton))];
	
	[containerView addConstraints:CONSTRAINTS(@"H:[logoutButton]-20-|", NSDictionaryOfVariableBindings(logoutButton))];
	[containerView addConstraints:CONSTRAINTS(@"V:|-20-[loginLabel][logoutButton]", NSDictionaryOfVariableBindings(headerLabel, loginLabel, logoutButton))];
}

- (void)setupConstraintsForCreateTodoTF
{
	// Width constraint
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:createTodoTF
														  attribute:NSLayoutAttributeWidth
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeWidth
														 multiplier:1
														   constant:0]];
	// Height constraint
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:createTodoTF
														  attribute:NSLayoutAttributeHeight
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeHeight
														 multiplier:0.1
														   constant:0]];
}

- (void)setupConstraintsForTableView
{
	// Width constraint
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:tableView
														  attribute:NSLayoutAttributeWidth
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeWidth
														 multiplier:1
														   constant:0]];
	// Height constraint
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:tableView
														  attribute:NSLayoutAttributeHeight
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeHeight
														 multiplier:0.70
														   constant:0]];
}

- (void)setupConstraintsForContainer
{
	// Width constraint
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:containerView
														  attribute:NSLayoutAttributeWidth
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeWidth
														 multiplier:1
														   constant:0]];
	// Height constraint
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:containerView
														  attribute:NSLayoutAttributeHeight
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeHeight
														 multiplier:0.2
														   constant:0]];
	
	// Place the view on the screen. 0.0 is upper left
	// start at left of the screen and relate to self.view.
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:containerView
														  attribute:NSLayoutAttributeLeft
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeLeft
														 multiplier:1.0
														   constant:0.0]];
	// start at the top of the screen and relate to self.view
	[self.view addConstraint:[NSLayoutConstraint constraintWithItem:containerView
														  attribute:NSLayoutAttributeTop
														  relatedBy:NSLayoutRelationEqual
															 toItem:self.view
														  attribute:NSLayoutAttributeTop
														 multiplier:1.0
														   constant:0.0]];
}

- (void)createConstrainsContainers
{
	[self.view addConstraints:CONSTRAINTS(@"V:|[containerView]-0-[createTodoTF]-0-[tableView]", NSDictionaryOfVariableBindings
										  (containerView, createTodoTF, tableView))];
}


@end
