//
//  Network.h
//  todo-ObjC
//
//  Created by Mathias Hansen on 23/02/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LoginViewController;

@interface Network : NSObject

-(void)login:(NSDictionary *)usernameandpassword withCallback:(void(^) (id, id))callback;

-(void)get:(void (^) (id, id))callback;

//- (void)abort;

-(void)addTodo:(NSString *)theTodo withCallback:(void (^)(NSError *, id))callback;

-(void)delete:(NSString *)objectToDelete;

@end
