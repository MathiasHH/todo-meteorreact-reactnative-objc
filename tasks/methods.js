Meteor.methods({

	methodCalledFromApp(arg1, arg2, arg3, arg4) {
		"use strict";
		console.log("Method called from iOS App");
		console.log(arguments.length);
		console.log("arguments : " + JSON.stringify(arguments, null, 4));
		console.log("arg2 : " + JSON.stringify(arg2, null, 4));
	}

});